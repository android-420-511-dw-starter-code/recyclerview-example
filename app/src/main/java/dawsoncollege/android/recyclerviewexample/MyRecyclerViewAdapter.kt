package dawsoncollege.android.recyclerviewexample

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import dawsoncollege.android.recyclerviewexample.databinding.PersonItemBinding

class MyRecyclerViewAdapter(private val listPersons: MutableList<Person>) :
    RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder>() {

    class ViewHolder(val binding: PersonItemBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // Here we create the binding from scratch, for a new ViewHolder
        val binding = PersonItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // Here we don't need to create the binding, we just take it from ViewHolder
        val binding = holder.binding
        val person = listPersons[position]

        binding.nameTextView.text = person.name
        binding.ageTextView.text = person.age.toString()
        binding.cakeTextView.text = if (person.likesCake) "yes" else "no"
    }

    override fun getItemCount(): Int = listPersons.size
}