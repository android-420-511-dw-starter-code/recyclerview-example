package dawsoncollege.android.recyclerviewexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import dawsoncollege.android.recyclerviewexample.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var mutableListOfPersons: MutableList<Person>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        /*
         * This is the datasource the Adapter will interact with.
         *
         * In this case, the datasource is a hardcoded MutableList of Person objects
         */
        mutableListOfPersons = DATABASE

        /*
         * This is the Adapter, it's the middle-man between the datasource and the AdapterView.
         *
         * The AdapterView uses the Adapter whenever it needs a new item (e.g. when the user scrolls
         * down and we need to show the next item in the list).
         * This is how it conceptually happens :
         * 1. AdapterView requests the next item from its Adapter
         * 2. The Adapter requests the next item from its datasource
         * 3. Given the new item from its datasource, the Adapter creates a View for that item
         * 4. The View is returned to the AdapterView
         *
         * In this case, the Adapter we use is MyRecyclerViewAdapter
         */
        val adapter = MyRecyclerViewAdapter(mutableListOfPersons)

        /*
         * This is the AdapterView, its responsibility is to display the View objects given to it by
         * its Adapter.
         *
         * In this case, the AdapterView we use is a RecyclerView
         */
        binding.myRecyclerView.adapter = adapter

        /*
         * Another difference with the other AdapterViews, is that you have to define a
         * LayoutManager that the RecyclerView wil use. I tells the RecyclerView in what manner to
         * display the Views it receives from its adapter.
         *
         * In this case we choose LinearLayoutManager, which displays the items in a 1D list
         */
        binding.myRecyclerView.layoutManager = LinearLayoutManager(this)
    }

}