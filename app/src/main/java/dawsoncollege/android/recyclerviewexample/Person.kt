package dawsoncollege.android.recyclerviewexample

data class Person(
    val name: String,
    val age: Int,
    val likesCake: Boolean = true
)

/**
 * Fake database for testing purposes
 */
val DATABASE: MutableList<Person> = mutableListOf(
    Person("John Doe", 45),
    Person("John Doe #2", 45),
    Person("Richard Stallman", 69),
    Person("Mary Sue", 20, likesCake = false),
    Person("Drkjwu'l, bringer of darkness", 9999, likesCake = false),
    Person("10101001010101001", 101)
).also { it.shuffle() }